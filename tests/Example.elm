module Example exposing (..)


import Expect exposing (true)
import Test exposing (..)


suite : Test
suite =
    describe "Stub for tests"
        [ test "Stub" <|
            \_ ->
                Expect.true "it's just True" True
        ]
